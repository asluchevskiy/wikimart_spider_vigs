# -*- coding: utf-8 -*-
import re
import logging
import datetime
import signal
import MySQLdb
import string
from pyquery import PyQuery
from grab.spider import Spider, Task
from grab.tools.text import find_number
from urllib import urlencode
from grab.tools.text import normalize_space
from models import TaxonomyTermData, TaxonomyTermHierarchy, ParsedTaxonomyFields, ParsedItems, \
    FieldConfig, FieldConfigInstance, BrandData, BrandInstance, ShopData, ShopInstance
from utils import make_path_from_url
try:
    import local_settings as settings
except ImportError:
    import settings

class WikimartBaseSpider(Spider):

    def shutdown(self):
        print self.render_stats()

    def sigint_handler(self, signal, frame):
        logging.info('Interrupting...')
        self.should_stop = True

    def prepare(self):
        signal.signal(signal.SIGINT, self.sigint_handler)
        if settings.PROXY_LIST:
            self.setup_proxylist(settings.PROXY_LIST, 'http', auto_change=False)
        if settings.USE_CACHE:
            self.setup_cache(database=settings.MONGO_DB)
        self.db = MySQLdb.connect(host=settings.MYSQL_HOST, user=settings.MYSQL_USER,
            passwd=settings.MYSQL_PASS, db=settings.MYSQL_DB, charset='utf8')
        self.cursor = self.db.cursor()
        self.now = datetime.datetime.now()

    def shutdown(self):
        self.db.close()

class WikimartItemsSpider(WikimartBaseSpider):

    field_ids = dict()

    def prepare(self):
        super(WikimartItemsSpider, self).prepare()
        for key in ('name', 'description', 'wikimartlink'):
            self.field_ids[key] = str(FieldConfig.objects.get(mname=key).id)  + '_value'
#
    def task_generator(self):
        for item in ParsedItems.objects.filter(is_parsed=False):
            yield Task('page', url=item.wikimartlink, data=item)
#
    def task_page(self, grab, task):
        if grab.response.code == 404:
            return
        try:
            pq = grab.pyquery
        except ValueError:
            yield task.clone(refresh_cache=True); return

        result = dict()
        name = pq.find('h1').text()
        result[self.field_ids['name']] = name
        if pq.find('div.Content div.description'):
            description = normalize_space(pq.find('div.Content div.description').html().strip())
            result[self.field_ids['description']] = description

        result[self.field_ids['wikimartlink']] = task.url

        brand_s = re.search('"bi":"(\d+)"', grab.response.body)
        try:
            result['brand_id'] = BrandData.objects.get(market_id=int(brand_s.group(1))).id
        except (BrandData.DoesNotExist, AttributeError):
            pass

        dl_list = pq.find('div.Props dl.ui-helper-clearfix')
        for i in range(len(dl_list)):
            field_mname = dl_list.eq(i).find('dt span').attr('class')
            if field_mname not in self.field_ids:
                try:
                    fc = FieldConfig.objects.get(mname=field_mname)
                except FieldConfig.DoesNotExist:
                    fc = FieldConfig(rusname=dl_list.eq(i).find('dt span').text(), mname=field_mname, type='string')
                    fc.save()
                    FieldConfigInstance(field_id=fc.id, taxonomy_id=task.data.taxonomy_id).save()
                    self.cursor.execute('ALTER TABLE products_%d ADD %d_value TEXT NULL DEFAULT NULL' % (task.data.taxonomy_id, fc.id))
                    self.db.commit()
                self.field_ids[field_mname] = '%s_value' % fc.id
            value = dl_list.eq(i).find('dd').text()
            if value == u'Нет':
                value = 0
            elif value == u'Да':
                value = 1
            result[self.field_ids[field_mname]] = value

        images = list()
        li_list = pq.find('ul.thumbnails li')
        for i in range(len(li_list)):
            images.append(li_list.eq(i).attr('data-gallery-image-url'))

        videos = list()
        div_list = pq.find('div.feedback[itemtype="http://schema.org/VideoObject"]')
        for i in range(len(div_list)):
            video_url = div_list.eq(i).find('meta[itemprop=url]').attr('content')
            if video_url:
                videos.append(video_url)

        sql = 'INSERT INTO products_%d(%s) VALUES(%s)' % (
            task.data.taxonomy_id,
            string.join(sorted(result), ', '),
            string.join([ '\'%s\'' % self.db.escape_string( unicode(result[key]) ) for key in sorted(result)], ', ')
            )
        self.cursor.execute(sql)
        product_id = self.db.insert_id()

        for url in images:
            self.cursor.execute('''INSERT INTO products_%d_links(product_id, linktype, linktext) VALUES(%d,1,'%s')''' %
                (task.data.taxonomy_id, product_id, url))
        for url in videos:
            self.cursor.execute('''INSERT INTO products_%d_links(product_id, linktype, linktext) VALUES(%d,2,'%s')''' %
                (task.data.taxonomy_id, product_id, url))

        self.db.commit()

        task.data.is_parsed = True
        task.data.timestamp = datetime.datetime.now()
        task.data.save()

class WikimartSpider(WikimartBaseSpider):

    initial_urls = ['http://wikimart.ru/catalog/']

    def task_initial(self, grab, task):
        pq = grab.pyquery
        weight = 0
        for a in pq.find('a.main-category'):
            s = re.search('http://([a-z]+)\.', a.get('href'))
            if not s:
                continue
            slug = s.group(1)
            curl = a.get('href')
            weight += 10
            name=unicode(a.text_content())
            if settings.CATEGORY and slug != settings.CATEGORY:
                continue
            try:
                d = TaxonomyTermData.objects.get(wikimartlink=curl)
            except TaxonomyTermData.DoesNotExist:
                d = TaxonomyTermData(name=name, pcid=slug, weight=weight, wikimartlink=curl)
                d.save()
            yield Task('category', url=curl, curl=curl, p=1, parent_id=d.id, cat=d)
        self.db.commit()

    def task_category(self, grab, task):
        pq = grab.pyquery
        divs = pq.find('div.ListGood')
        if not task.cat.market_id:
            task.cat.market_id = int(re.search('"ci":"(\d+)"', grab.response.body).group(1))
            task.cat.save()
        if not divs:
            a_list = pq.find('h3.title a')
        else:
            if pq.find('h2.HeadTit').text() == u'Подкатегории' and task.p==1:
                a_list = pq.find('ul.Category li.level-1 a')
            else:
                a_list = []
        weight = 0
        for a in a_list:
            curl = grab.make_url_absolute(a.get('href')).split('?')[0]
            slug = make_path_from_url(curl)[-1]
            weight += 10
            name = unicode(a.text_content())
            try:
                d = TaxonomyTermData.objects.get(wikimartlink=curl)
            except TaxonomyTermData.DoesNotExist:
                d = TaxonomyTermData(name=name, pcid=slug, weight=weight, wikimartlink=curl)
                d.save()
                TaxonomyTermHierarchy(taxonomy_parent=task.parent_id, taxonomy_id=d.id).save()
            yield Task('category', url=curl+'?'+urlencode({'p':1,'list':1,'geodata[city_id]':1}), p=1,
                curl=curl, parent_id=d.id, cat=d)
        # мы дошли до последнего уровня подкатегории
        if divs and not a_list:
            if task.p == 1:
                try:
                    ParsedTaxonomyFields.objects.get(taxonomy_id=task.parent_id)
                except ParsedTaxonomyFields.DoesNotExist:
                    yield Task('extended', url=task.curl.strip('/')+'/extended/', id=task.parent_id)
            # товары
            for i in xrange(len(divs)):
                div = divs.eq(i)
                model_id = int(div.attr('model_id'))
                url = div.find('div.InfoModel a').attr('href')
                try:
                    ParsedItems.objects.get(model_id=model_id)
                except ParsedItems.DoesNotExist:
                    ParsedItems(model_id=model_id, wikimartlink=url, taxonomy_id=task.parent_id).save()
            # следующая страница каталога
            yield Task('category', url=task.curl+'?'+urlencode({'p':task.p+1,'list':1,'geodata[city_id]':1}),
                p=task.p+1, curl=task.curl, parent_id=task.parent_id, cat=task.cat)

    def task_extended(self, grab, task):
        pq = PyQuery(grab.response.body.decode('UTF-8'))
        # TODO? : типы
        for a in pq.find('div#brand a.non_selected_property'):
            brand_id = int(a.get('value'))
            try:
                b = BrandData.objects.get(market_id=brand_id)
            except BrandData.DoesNotExist:
                b = BrandData(name=a.text_content(), market_id=brand_id)
                b.save()
            BrandInstance.objects.get_or_create(brand_id=b.id, taxonomy_id=task.id)
        for mname in ('name', 'description', 'wikimartlink'):
            d = FieldConfig.objects.get_or_create(mname=mname, type='text')[0]
            FieldConfigInstance(field_id=d.id, taxonomy_id=task.id).save()
        div = pq.find('div#props div.contaner')

        for i in range(len(div.children('label'))):
            rusname = div.children('label').eq(i).text().split(',')
            rusname = rusname[0]
            unit = rusname[1].strip() if len(rusname) == 2 else None
            select = div.children('div').eq(i).find('select')
            boolean = div.children('div').eq(i).find('ul.bot_ot')
            numeric = div.children('div').eq(i).find('div.Pric')
            if select:
                type = 'list_text'
                mname = 'property_' + select.attr('name').strip('[]')
                additional =  string.join([s.get('title') for s in select.find('option')], ',')
            elif boolean:
                type = 'boolean'
                mname = 'property_' + boolean.find('input').attr('name')
                additional = None
            elif numeric:
                type = 'float'
                mname = 'property_' + numeric.find('input').attr('name')
                additional = '%s,%s' % (numeric.find('div.min').text(), numeric.find('div.max').text())
            try:
                d = FieldConfig.objects.get(mname=mname)
            except FieldConfig.DoesNotExist:
                d = FieldConfig(rusname=rusname, mname=mname, type=type, unit=unit, additional=additional)
                d.save()
            FieldConfigInstance(field_id=d.id, taxonomy_id=task.id).save()

        ParsedTaxonomyFields(taxonomy_id=task.id).save()
        self._create_tables(task.id)

    def _create_tables(self, taxonomy_id):
        sql_part = ''
        for instance in FieldConfigInstance.objects.filter(taxonomy_id=taxonomy_id).order_by('field_id'):
            fc = FieldConfig.objects.get(id=instance.field_id)
            if fc.type in ('text', 'list_text'):
                type = 'text'
            elif fc.type == 'float':
                type = 'float'
            elif fc.type =='boolean':
                type = 'boolean'
            else:
                continue
            sql_part += '%s_value %s DEFAULT NULL,\n' % (fc.id, type)
        sql = '''
CREATE TABLE IF NOT EXISTS `products_%d` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) DEFAULT NULL,
  %s
  `timestampParsed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `exported` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ''' % (taxonomy_id, sql_part)
        self.cursor.execute(sql)
        sql = '''
CREATE TABLE IF NOT EXISTS `products_%d_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `linktype` smallint(6) NOT NULL,
  `linktext` varchar(4000) NOT NULL,
  `timestampParsed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `exported` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Куча таблиц с названием вида products_[taxonomy_term_data.id' AUTO_INCREMENT=1 ;
        ''' % taxonomy_id
        self.cursor.execute(sql)
        self.db.commit()

class WikimartShoupsSpider(WikimartBaseSpider):

    initial_urls = ['http://wikimart.ru/shops/']

    months = {
        u'января' : 1,
        u'февраля' : 2,
        u'марта' : 3,
        u'апреля' : 4,
        u'мая' : 5,
        u'июня' : 6,
        u'июля' : 7,
        u'августа' : 8,
        u'сентября' : 9,
        u'октября' : 10,
        u'ноября' : 11,
        u'декабря' : 12,
    }

    def task_initial(self, grab, task):
        pq = grab.pyquery
        next_href = pq.find('div.next a').attr('href')
        if next_href:
            yield Task('initial', grab.make_url_absolute(next_href))
        li_list = pq.find('div.ShopList div.cont').children('ul').children('li')
        for i in range(len(li_list)):
            li = li_list.eq(i)
            name = li.find('h2 a').text()
            url = grab.make_url_absolute( li.find('h2 a').attr('href') )
            market_id = int(find_number(url))
            try:
                rating = int(find_number( li.find('h2 div').attr('class') ))
            except TypeError:
                rating = 0
            date_raw = li.find('p.from').text()
            year = int(find_number(date_raw))
            month = self.months.get( date_raw.split(u' с ')[1].split()[0] )
            from_date = datetime.date(year=year, month=month, day=1)
            try:
                goods_count = int(li.find('td.all_good i').text())
            except TypeError:
                goods_count = 0
            try:
                sh = ShopData.objects.get(market_id=market_id)
            except ShopData.DoesNotExist:
                sh = ShopData(name=name, wikimartlink=url, market_id=market_id,
                    from_date=from_date, goods_count=goods_count, rating=rating,
                    ctime=datetime.datetime.now(), mtime=datetime.datetime.now())
                sh.save()
            yield Task('shop', url, shop_id=market_id)
#            break

    def task_shop(self, grab, task):
        pq = grab.pyquery
        for a in pq.find('td.categ a'):
            href = a.get('href')
            category_id = int(re.search('goods/(\d+)/', href).group(1))
            try:
                t = TaxonomyTermData.objects.get(market_id=category_id)
                ShopInstance.objects.get_or_create(shop_id=task.shop_id, taxonomy_id=t.id)
            except TaxonomyTermData.DoesNotExist:
                logging.error('Could not find TaxonomyTermData with market_id=%s' % category_id)
                continue


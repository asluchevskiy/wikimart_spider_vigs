# -*- coding: utf-8 -*-
import os
import sys
import logging
import locale
import optparse
from WikimartSpider import WikimartSpider, WikimartItemsSpider, WikimartShoupsSpider

try:
    import local_settings as settings
except ImportError:
    import settings

def run_categories():
    WikimartSpider(thread_number=settings.THREADS).run()

def run_items():
    WikimartItemsSpider(thread_number=settings.THREADS).run()

def run_shops():
    WikimartShoupsSpider(thread_number=settings.THREADS).run()

if __name__ == '__main__':
    # hack for unicode console i/o
    reload(sys)
    sys.setdefaultencoding(locale.getpreferredencoding())
    if settings.DEBUG:
        logging.basicConfig(level=logging.DEBUG)

    parser = optparse.OptionParser("usage: %prog [options]")
    parser.add_option('-m', '--mode', action="store", dest="mode", default=None,
        help='mode to run: all|categories|items|shops')
    parser.add_option('-c', '--category', action="store", dest="category", default=None, help='category')
    (options, args) = parser.parse_args()

    settings.CATEGORY = options.category
    if options.mode == 'all':
        run_categories(); run_items();
    if options.mode == 'categories':
        run_categories()
    elif options.mode == 'items':
        run_items()
    elif options.mode == 'shops':
        run_shops()
    else:
        parser.print_help(); exit()

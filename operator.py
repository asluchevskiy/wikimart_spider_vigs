# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
from flask import Flask, render_template, g, abort, request
try:
    import local_settings as settings
except ImportError:
    import settings
from models import ShopData, ShopInstance

app = Flask(__name__)

def get_shop_list():
    return ShopData.objects.all().order_by('-rating', '-goods_count')

@app.route('/all/')
def index_all():
    return render_template('index.html', shop_list=get_shop_list())

@app.route('/')
def index():
    return render_template('index.html', shop_list=get_shop_list().filter(status=None))

@app.route('/shop/<shop_id>/', methods=['GET', 'POST'])
def shop(shop_id):
    try:
        shop = ShopData.objects.get(id=shop_id)
    except ShopData.DoesNotExist:
        abort(404)
    if request.method == 'POST':
        try:
            shop.comment = request.form['comment']
            if request.form['button'] in ('0', '1', '2'):
                shop.status = int(request.form['button'])
            else:
                shop.status = None
            shop.save()
        except KeyError:
            pass
    shop_list = list()
    for sh in get_shop_list():
        if sh.id == shop.id or sh.status == None:
            shop_list.append(sh)
    i = 0
    for sh in shop_list:
        if sh.id == shop.id:
            break
        i+=1
    prev = shop_list[i-1].id if i else None
    next = shop_list[i+1].id if i+1 < len(shop_list) else None
    return render_template('shop.html', shop=shop, prev=prev, next=next)

if __name__ == "__main__":
    app.run(debug = True, host = '0.0.0.0')

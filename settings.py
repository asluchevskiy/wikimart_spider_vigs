# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
import os

#PROXY_LIST = os.path.join(os.path.dirname(__file__), 'ru_proxy.list')
PROXY_LIST = None

USE_CACHE = False
MONGO_DB = 'wikimart'

CATEGORY = None # all categories to parse

DEBUG = True
THREADS = 32

MYSQL_HOST = 'localhost'
MYSQL_USER = 'root'
MYSQL_PASS = ''
MYSQL_DB = 'wikimart'

-- phpMyAdmin SQL Dump
-- version 3.3.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Окт 13 2012 г., 16:35
-- Версия сервера: 5.1.63
-- Версия PHP: 5.3.5-1ubuntu7.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `wikimart`
--

-- --------------------------------------------------------

--
-- Структура таблицы `brand_data`
--

CREATE TABLE IF NOT EXISTS `brand_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brandName` varchar(128) NOT NULL,
  `brandValueAttribute` int(11) NOT NULL,
  `timestampParsed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `brandValueAttribute` (`brandValueAttribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `brand_instance`
--

CREATE TABLE IF NOT EXISTS `brand_instance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL,
  `timestampParsed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `exported` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `field_config`
--

CREATE TABLE IF NOT EXISTS `field_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'номер записи в таблице',
  `rusname` varchar(128) DEFAULT NULL COMMENT 'Название поля на русском языке',
  `mname` varchar(32) NOT NULL COMMENT 'машинное имя поля',
  `type` varchar(128) NOT NULL COMMENT 'тип поля',
  `unit` varchar(32) DEFAULT NULL COMMENT 'Единица измерения',
  `additionalData` text COMMENT 'Дополнительная информация',
  `timestampParsed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время парсинга данной записи',
  `exported` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Флаг указывающий была ли данная запись экспортирована в БД Drupal',
  PRIMARY KEY (`id`),
  KEY `mname` (`mname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Описание свойств товаров' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `field_config_instance`
--

CREATE TABLE IF NOT EXISTS `field_config_instance` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'намер записи в таблице',
  `field_id` int(11) NOT NULL COMMENT '[field_config.id] Ссылка на запись в таблице field_config',
  `taxonomy_id` int(11) NOT NULL COMMENT '[taxonomy_term_data.id] Ссылка на запись в таблице taxonomy_term_data',
  `timestampParsed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время парсинга данной записи',
  `exported` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Флаг указывающий была ли данная запись экспортирована в БД Drupal',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Связь между классами товаров и свойствами товаров' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `parsed_items`
--

CREATE TABLE IF NOT EXISTS `parsed_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL,
  `wikimartlink` varchar(4000) NOT NULL,
  `is_parsed` tinyint(1) NOT NULL DEFAULT '0',
  `timestampParsed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `model_id` (`model_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58066 ;

-- --------------------------------------------------------

--
-- Структура таблицы `parsed_taxonomy_fields`
--

CREATE TABLE IF NOT EXISTS `parsed_taxonomy_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taxonomy_id` int(11) NOT NULL,
  `timestampParsed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_data`
--

CREATE TABLE IF NOT EXISTS `shop_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `comment` text NOT NULL,
  `wikimartlink` varchar(64) NOT NULL,
  `market_id` int(11) NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `from_date` date NOT NULL,
  `goods_count` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mtime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `market_id` (`market_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1487 ;

-- --------------------------------------------------------

--
-- Структура таблицы `shop_instance`
--

CREATE TABLE IF NOT EXISTS `shop_instance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `taxonomy_id` int(11) NOT NULL,
  `ctime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33605 ;

-- --------------------------------------------------------

--
-- Структура таблицы `taxonomy_term_data`
--

CREATE TABLE IF NOT EXISTS `taxonomy_term_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'номер записи в таблице',
  `name` varchar(255) NOT NULL COMMENT 'Название элемента каталога',
  `pcid` varchar(255) NOT NULL COMMENT 'Часть ссылки на элемент каталога на сайте Викимарт',
  `weight` int(11) NOT NULL COMMENT 'Вес элемента каталога. Чем меньше - тем раньше идет этот термин',
  `wikimartlink` varchar(4000) NOT NULL COMMENT 'ссылка на страницу Викимарта',
  `market_id` int(11) DEFAULT NULL,
  `timestampParsed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время парсинга данной записи',
  `exported` tinyint(1) NOT NULL COMMENT 'Флаг указывающий была ли данная запись экспортирована в БД Drupal',
  PRIMARY KEY (`id`),
  KEY `wikimartlink` (`wikimartlink`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Данные каталога: здесь хранятся все подряд категории' AUTO_INCREMENT=2836 ;

-- --------------------------------------------------------

--
-- Структура таблицы `taxonomy_term_hierarchy`
--

CREATE TABLE IF NOT EXISTS `taxonomy_term_hierarchy` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'номер записи в таблице',
  `taxonomy_id` int(11) NOT NULL COMMENT '[taxonomy_term_data.id] Ссылка на запись в таблице taxonomy_term_data',
  `taxonomy_parent` int(11) NOT NULL COMMENT '[taxonomy_term_data.id] Ссылка на запись в таблице taxonomy_term_data',
  `timestampParsed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Время парсинга данной записи',
  `exported` tinyint(1) NOT NULL COMMENT 'Флаг указывающий была ли данная запись экспортирована в БД Drupal',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Иерархическая связь между категориями товаров' AUTO_INCREMENT=2809 ;

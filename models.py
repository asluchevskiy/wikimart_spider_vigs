# -*- coding: utf-8 -*-
"""
Developed by Arseniy Sluchevskiy <arseniy.sluchevskiy@gmail.com>
"""
from datetime import datetime
from django.conf import settings as django_settings
try:
    import local_settings as settings
except ImportError:
    import settings

django_settings.configure(
    DATABASES={
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': settings.MYSQL_DB,
            'USER': settings.MYSQL_USER,
            'PASSWORD': settings.MYSQL_PASS,
            'HOST': settings.MYSQL_HOST,
            'PORT': '',
        }
    },
    STANDALONE_APP_NAME='wikimart',
)

from django.db import models

class StandAloneMetaclass(models.base.ModelBase):
    """Метакласс, который задает app_label моделям используя
    значение из STANDALONE_APP_NAME"""
    def __new__(cls, name, bases, attrs):
        app_name = django_settings.STANDALONE_APP_NAME or None
        if app_name:
            if 'Meta' in attrs:
                if not hasattr(attrs['Meta'], 'app_label'):
                    # устанавливаем app_label только если он уже не указан в самой модели
                    setattr(attrs['Meta'], 'app_label', app_name)
        return super(StandAloneMetaclass, cls).__new__(cls, name, bases, attrs)

class TaxonomyTermData(models.Model):
    __metaclass__ = StandAloneMetaclass
    name = models.CharField(max_length=255)
    pcid = models.CharField(max_length=255)
    weight = models.IntegerField()
    wikimartlink = models.CharField(max_length=4000)
    market_id = models.IntegerField(default=None, null=True)
    timestamp = models.DateTimeField(default=datetime.now(), db_column='timestampParsed')
    exported = models.BooleanField(default=False)
    class Meta:
        db_table = u'taxonomy_term_data'

class TaxonomyTermHierarchy(models.Model):
    __metaclass__ = StandAloneMetaclass
    taxonomy_id	= models.IntegerField()
    taxonomy_parent	= models.IntegerField()
    timestamp = models.DateTimeField(default=datetime.now(), db_column='timestampParsed')
    exported = models.BooleanField(default=False)
    class Meta:
        db_table = u'taxonomy_term_hierarchy'

class ParsedTaxonomyFields(models.Model):
    __metaclass__ = StandAloneMetaclass
    taxonomy_id	= models.IntegerField()
    timestamp = models.DateTimeField(default=datetime.now(), db_column='timestampParsed')
    class Meta:
        db_table = u'parsed_taxonomy_fields'

class ParsedItems(models.Model):
    __metaclass__ = StandAloneMetaclass
    model_id = models.IntegerField()
    wikimartlink = models.CharField(max_length=4000)
    taxonomy_id = models.IntegerField()
    is_parsed = models.BooleanField(default=False)
    timestamp = models.DateTimeField(default=datetime.now(), db_column='timestampParsed')
    class Meta:
        db_table = u'parsed_items'

class FieldConfig(models.Model):
    __metaclass__ = StandAloneMetaclass
    rusname	= models.CharField(max_length=128, null=True, blank=True)
    mname = models.CharField(max_length=32)
    type = models.CharField(max_length=128)
    unit = models.CharField(max_length=32, null=True, blank=True)
    additional = models.TextField(db_column='additionalData', null=True, blank=True)
    timestamp = models.DateTimeField(default=datetime.now(), db_column='timestampParsed')
    exported = models.BooleanField(default=False)
    class Meta:
        db_table = u'field_config'

class FieldConfigInstance(models.Model):
    __metaclass__ = StandAloneMetaclass
    field_id = models.IntegerField()
    taxonomy_id = models.IntegerField()
    timestamp = models.DateTimeField(default=datetime.now(), db_column='timestampParsed')
    exported = models.BooleanField(default=False)
    class Meta:
        db_table = u'field_config_instance'

class ShopData(models.Model):
    __metaclass__ = StandAloneMetaclass
    name = models.CharField(max_length=64)
    wikimartlink = models.CharField(max_length=64)
    market_id = models.IntegerField()
    rating = models.IntegerField(null=True, default=None)
    from_date = models.DateField()
    goods_count = models.IntegerField()
#    status = models.NullBooleanField()
    status = models.IntegerField(null=True, default=None)
    comment = models.TextField(default='')
    ctime = models.DateTimeField(default=datetime.now())
    mtime = models.DateTimeField(default=datetime.now())
    class Meta:
        db_table = u'shop_data'

class ShopInstance(models.Model):
    __metaclass__ = StandAloneMetaclass
    shop_id = models.IntegerField()
    taxonomy_id = models.IntegerField()
    ctime = models.DateTimeField(default=datetime.now())
    class Meta:
        db_table = u'shop_instance'

class BrandData(models.Model):
    __metaclass__ = StandAloneMetaclass
    name = models.CharField(max_length=128, db_column='brandName')
    market_id = models.IntegerField(db_column='brandValueAttribute')
    timestamp = models.DateTimeField(default=datetime.now(), db_column='timestampParsed')
    class Meta:
        db_table = u'brand_data'

class BrandInstance(models.Model):
    __metaclass__ = StandAloneMetaclass
    brand_id = models.IntegerField()
    taxonomy_id = models.IntegerField()
    timestamp = models.DateTimeField(default=datetime.now(), db_column='timestampParsed')
    exported = models.BooleanField(default=False)
    class Meta:
        db_table = u'brand_instance'

def create_tables(tables):
    from django.db import connection, transaction
    from django.core.management.color import no_style
    pending_references = {}  # список внешних колючей, ожидающих создания связываемых таблиц
    seen_models = set()  # список созданных
    style = no_style()  # указывает, что не нужно раскрашивать синтаксис сгенерированного SQL
    for model in tables:  # проходим по списку моделей
        sql, references = connection.creation.sql_create_model(model, style) # генерируем SQL и объекты внешних ключей
        seen_models.add(model)
        for refto, refs in references.items():
            pending_references.setdefault(refto, []).extend(refs)
            if refto in seen_models:
                sql.extend(  # если все таблицы внешнего ключа существуют, то добавляем сам ключ
                    connection.creation.sql_for_pending_references(
                            refto,
                            style,
                            pending_references))
        sql.extend(
            connection.creation.sql_for_pending_references(
                model,
                style,
                pending_references))
        cursor = connection.cursor()
        for stmt in sql:  # выполняем запросы к БД
            print stmt
            try:
                cursor.execute(stmt)
            except Exception, e:
                print e
                pass
        print
    transaction.commit_unless_managed()  # завершаем транзакцию
    for model in tables:  # создаем индексы
        index_sql = connection.creation.sql_indexes_for_model(model, style)
        if index_sql:
            try:
                for stmt in index_sql:
                    print stmt
                    cursor.execute(stmt)
            except Exception, e:
                transaction.rollback_unless_managed()
            else:
                transaction.commit_unless_managed()

if __name__ == "__main__":
    # находим все классы Django моделей в локальной области видимости и создаем для них
    # таблицы в БД
    create_tables(mymodel for mymodel in locals().values() if type(mymodel) == StandAloneMetaclass)
